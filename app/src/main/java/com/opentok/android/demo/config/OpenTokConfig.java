package com.opentok.android.demo.config;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    // Replace with a generated Session ID
    public static final String SESSION_ID = "1_MX40NTY4OTE1Mn5-MTQ3NDU0MjY5Nzc2Nn5CRDdLbmR5RWhBZk1UY1ZON08yTlZoK0h-fg";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static final String TOKEN = "T1==cGFydG5lcl9pZD00NTY4OTE1MiZzaWc9ZWFlNmVlNGJjZDZiZjhhY2NmZjE0MTg3ZDQ3ODk3NTFkMjY2ZDAzMTpzZXNzaW9uX2lkPTFfTVg0ME5UWTRPVEUxTW41LU1UUTNORFUwTWpZNU56YzJObjVDUkRkTGJtUjVSV2hCWmsxVVkxWk9OMDh5VGxab0swaC1mZyZjcmVhdGVfdGltZT0xNDc0NTQyNzEzJm5vbmNlPTAuMzEzMjA5Nzg0OTE1NjcwNzUmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTQ3NDU0NjMxMg==";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45689152";

    // Subscribe to a stream published by this client. Set to false to subscribe
    // to other clients' streams only.
    public static final boolean SUBSCRIBE_TO_SELF = false;


    public static String MSESSION_ID = "sessionId";
    public static String MTOKEN_ID = "tokenId";
    public static final String MAPI_KEY = "45694412";
}
