package com.opentok.android.demo.opentoksamples;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.opentok.android.demo.config.OpenTokConfig;
import com.opentok.android.demo.config.PrefManager;

/**
 * Created by jithi on 22-09-2016.
 */
public class SettingsActivity extends  Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        findViewById(R.id.submit).setOnClickListener(this);
    }

    private EditText getEditTextsession(){
        return (EditText) findViewById(R.id.editTextsession);
    }

    private EditText getEditTexttoken(){
        return (EditText) findViewById(R.id.editTexttoken);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if(getEditTextsession().getText()!=null) {
                    OpenTokConfig.MSESSION_ID = getEditTextsession().getText() + "";
                    PrefManager.getInstance(getApplicationContext()).saveSessionId(    OpenTokConfig.SESSION_ID );
                }
                if(getEditTexttoken().getText()!=null) {
                    OpenTokConfig.MTOKEN_ID = getEditTexttoken().getText() + "";
                    PrefManager.getInstance(getApplicationContext()).saveTokenId( OpenTokConfig.TOKEN);
                }
                finish();
                break;
        }
    }
}

