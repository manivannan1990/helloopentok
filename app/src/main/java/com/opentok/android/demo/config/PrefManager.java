package com.opentok.android.demo.config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;

public class PrefManager {


    public static final String PREFERENCES = "Login" ;
    public static final String session = "session";
    public static final String tokenid = "token";


    Context context;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    private static PrefManager instance = null;
    PrefManager()
    {

    }
    public static PrefManager getInstance(Context context) {
        if(instance == null) {
            instance = new PrefManager(context);
        }
        return instance;
    }
    public PrefManager(Context context) {
        super();
        this.context = context;
        sharedpreferences=context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        editor=sharedpreferences.edit();
    }

 /*   void sharedSaveUser(LoginPostResponse login)
    {
        //sharedpreferences =context.getSharedPreferences(MYPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(isValid(login.getUserid()))
            editor.putString(USERID, login.getUserid());

        editor.commit();
    }
*/


    public String getSessionId()
    {
        return getSharedString(session, "");

    }
    public String getTokenId()
    {
        return getSharedString(tokenid, "");

    }
    public void saveTokenId(String token_id) {
        putSharedString(tokenid,token_id);
        OpenTokConfig.MTOKEN_ID = getTokenId();
    }
    public void saveSessionId(String sessionid) {
        putSharedString(session,sessionid);
        OpenTokConfig.MSESSION_ID = getSessionId();
    }


    public void onRefresh() {

        OpenTokConfig.MSESSION_ID = getSessionId();
        OpenTokConfig.MTOKEN_ID = getTokenId();
    }





    public String getSharedString(String KEY, String defValue) {
        if(!isValid(sharedpreferences))
            sharedpreferences =context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if(!isValid(editor))
            editor = sharedpreferences.edit();
        return sharedpreferences.getString(KEY, defValue);
    }

    public void putSharedString(String KEY, String value) {
        if(!isValid(sharedpreferences))
            sharedpreferences =context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if(!isValid(editor))
            editor = sharedpreferences.edit();
        editor.putString(KEY, value).commit();
    }

    public boolean getSharedBoolean(String KEY, boolean defValue) {
        if(!isValid(sharedpreferences))
            sharedpreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if(!isValid(editor))
            editor = sharedpreferences.edit();
        return sharedpreferences.getBoolean(KEY, defValue);
    }

    public void putSharedBoolean(String KEY, boolean value) {
        if(!isValid(sharedpreferences))
            sharedpreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if(!isValid(editor))
            editor = sharedpreferences.edit();
        editor.putBoolean(KEY, value).commit();
    }

    private Long getSharedLong(String KEY) {
        if(!isValid(sharedpreferences))
            sharedpreferences =context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if(!isValid(editor))
            editor = sharedpreferences.edit();
        return sharedpreferences.getLong(KEY,0L);
    }

    private void putSharedLong(String KEY, long value) {
        if(!isValid(sharedpreferences))
            sharedpreferences =context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if(!isValid(editor))
            editor = sharedpreferences.edit();
        editor.putLong(KEY, value).commit();
    }


    public void clearSharedAll() {
        if(!isValid(editor))
        {
            if(!isValid(sharedpreferences))
                sharedpreferences =context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
            editor = sharedpreferences.edit();
        }
        editor.clear().commit();
    }



  


    public void clearAll() {
        editor.clear().commit();
    }



    private String getString(String KEY, String defValue) {
        return sharedpreferences.getString(KEY, defValue);
    }

    private void putString(String KEY, String value) {

        editor.putString(KEY, value).commit();
    }


    private Long getLong(String KEY) {
        return sharedpreferences.getLong(KEY,0L);
    }

    private void putLong(String KEY, long value) {

        editor.putLong(KEY, value).commit();
    }


    public Boolean isValid(Object text)
    {
        if(text!=null)

            return  true;
        return  false;

    }
    public Boolean isValid(String text)
    {
        if(text!=null)
            if(!text.trim().equalsIgnoreCase(""))
                return  true;
        return  false;

    }
    public Boolean isValid(List list)
    {
        if(list!=null)
            if(list.size()>0)
                return  true;
        return  false;

    }
}
